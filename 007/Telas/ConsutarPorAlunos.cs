﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace _007.Telas
{
    public partial class ConsutarPorAlunos : Form
    {
        public ConsutarPorAlunos()
        {
            InitializeComponent();
        }

        private void TextBox1_TextChanged(object sender, EventArgs e)
        {
            
        }

        private void TxtTurma_TextChanged(object sender, EventArgs e)
        {
            string turma = txtTurma.Text;
            int aluno = Convert.ToInt32(nudQtDeAlunos.Value);

            Bussi.Vali vali = new Bussi.Vali();
            List<BD.Enti.tb_turma> tb_Turmas = vali.ConstarPorAlunos(turma, aluno);

            dgvCons.DataSource = tb_Turmas;
        }
    }
}
