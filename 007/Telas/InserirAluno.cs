﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace _007.Telas
{
    public partial class InserirAluno : Form
    {
        public InserirAluno()
        {
            InitializeComponent();
        }

        private void Button1_Click(object sender, EventArgs e)
        {
            BD.Enti.tb_aluno tb_Aluno = new BD.Enti.tb_aluno
            {
                id_turma = Convert.ToInt32(nudIdTurma.Value),
                nr_chamada = Convert.ToInt32(nudNumeroChamada.Value),
                nm_aluno = txtAluno.Text,
                ds_bairro = txtBairro.Text,
                ds_municipio = txtMunicipio.Text,
                dt_nascimento = dtpDataNascimento.Value
            };

            Bussi.Vali vali = new Bussi.Vali();
            vali.InserirAluno(tb_Aluno);

            MessageBox.Show("Sucesso");
        }
    }
}
