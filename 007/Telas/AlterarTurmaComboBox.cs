﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace _007.Telas
{
    public partial class AlterarTurmaComboBox : Form
    {
        public AlterarTurmaComboBox()
        {
            InitializeComponent();

            Bussi.Vali vali = new Bussi.Vali();
            List<BD.Enti.tb_turma> tb_Turmas = vali.Listar();

            cboTurma.DisplayMember = nameof(BD.Enti.tb_turma.nm_turma);
            cboTurma.DataSource = tb_Turmas;
        }

        

        private void Button1_Click(object sender, EventArgs e)
        {
            int id = Convert.ToInt32(txtId.Text);

            Bussi.Vali vali = new Bussi.Vali();
            BD.Enti.tb_turma tb_Turma = vali.BuscarPorId(id);

            txtcurso.Text = tb_Turma.nm_curso;
            nudqt.Value = tb_Turma.qt_max_alunos;
        }

        private void Button2_Click(object sender, EventArgs e)
        {
            BD.Enti.tb_turma tb_Turma = new BD.Enti.tb_turma
            {
                id_turma = Convert.ToInt32(txtId.Text),
                nm_curso = txtcurso.Text,
                nm_turma = cboTurma.Text,
                qt_max_alunos = Convert.ToInt32(nudqt.Value)
            };

            Bussi.Vali vali = new Bussi.Vali();
            vali.Altera(tb_Turma);


            MessageBox.Show("Turma alterada com sucesso");
        }
    }
}
