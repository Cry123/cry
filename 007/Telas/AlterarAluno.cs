﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace _007.Telas
{
    public partial class AlterarAluno : Form
    {
        public AlterarAluno()
        {
            InitializeComponent();
        }

        private void Button1_Click(object sender, EventArgs e)
        {
            int id = Convert.ToInt32(txtBuscarId.Text);

            Bussi.Vali vali = new Bussi.Vali();
            BD.Enti.tb_aluno aluno = vali.BuscarPorIdAluno(id);

            txtAluno.Text = aluno.nm_aluno;
            txtBairro.Text = aluno.ds_bairro;
            nudIdTurma.Value = aluno.id_turma;
            nudNumeroChamada.Value = aluno.nr_chamada;
            txtMunicipio.Text = aluno.ds_municipio;
            dtpDataNascimento.Value = aluno.dt_nascimento;
        }

        private void Button2_Click(object sender, EventArgs e)
        {
            BD.Enti.tb_aluno tb_Aluno = new BD.Enti.tb_aluno();

            tb_Aluno.id_aluno = Convert.ToInt32(txtBuscarId.Text);
            tb_Aluno.id_turma = Convert.ToInt32(nudIdTurma.Value);
            tb_Aluno.nm_aluno = txtAluno.Text;
            tb_Aluno.nr_chamada = Convert.ToInt32(nudNumeroChamada.Value);
            tb_Aluno.ds_bairro = txtBairro.Text;
            tb_Aluno.ds_municipio = txtMunicipio.Text;
            tb_Aluno.dt_nascimento = dtpDataNascimento.Value;
            

            Bussi.Vali vali = new Bussi.Vali();
            vali.AlteraAluno(tb_Aluno);

            MessageBox.Show("Alterado");
        }
    }
}
