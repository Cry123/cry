﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace _007.Telas
{
    public partial class AlterarPelaTurma : Form
    {
        public AlterarPelaTurma()
        {
            InitializeComponent();
        }

        private void Button1_Click(object sender, EventArgs e)
        {
            int id = Convert.ToInt32(txtId.Text);

            Bussi.Vali vali = new Bussi.Vali();
            BD.Enti.tb_turma tb_Turma = vali.BuscarPorId(id);

            txtcurso.Text = tb_Turma.nm_curso;
            txtturma.Text = tb_Turma.nm_turma;
            nudqt.Value = tb_Turma.qt_max_alunos;
        }

        private void Button2_Click(object sender, EventArgs e)
        {
            BD.Enti.tb_turma tb_Turma = new BD.Enti.tb_turma
            {
                id_turma = Convert.ToInt32(txtId.Text),
                nm_curso = txtcurso.Text,
                nm_turma = txtturma.Text,
                qt_max_alunos = Convert.ToInt32(nudqt.Value)
            };

            Bussi.Vali vali = new Bussi.Vali();
            vali.Altera(tb_Turma);


            MessageBox.Show("Turma alterada com sucesso");
        }
    }
}
