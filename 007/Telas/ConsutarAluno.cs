﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace _007.Telas
{
    public partial class ConsutarAluno : Form
    {
        public ConsutarAluno()
        {
            InitializeComponent();

            Bussi.Vali vali = new Bussi.Vali();
            List<BD.Enti.tb_aluno> alunos = vali.ListarAlunos();

            dgvTudo.DataSource = alunos;
        }
    }
}
