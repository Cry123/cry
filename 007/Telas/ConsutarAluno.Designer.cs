﻿namespace _007.Telas
{
    partial class ConsutarAluno
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dgvTudo = new System.Windows.Forms.DataGridView();
            ((System.ComponentModel.ISupportInitialize)(this.dgvTudo)).BeginInit();
            this.SuspendLayout();
            // 
            // dgvTudo
            // 
            this.dgvTudo.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvTudo.Location = new System.Drawing.Point(12, 12);
            this.dgvTudo.Name = "dgvTudo";
            this.dgvTudo.Size = new System.Drawing.Size(754, 425);
            this.dgvTudo.TabIndex = 0;
            // 
            // ConsutarAluno
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(778, 449);
            this.Controls.Add(this.dgvTudo);
            this.Name = "ConsutarAluno";
            this.Text = "ConsutarAluno";
            ((System.ComponentModel.ISupportInitialize)(this.dgvTudo)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView dgvTudo;
    }
}