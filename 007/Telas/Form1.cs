﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace _007
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Label4_Click(object sender, EventArgs e)
        {
            try
            {
                Bussi.Vali valid1 = new Bussi.Vali();

                string turma = txtTurma.Text;

                BD.Enti.tb_turma tb_Turma1 = valid1.BuscarTurma(turma);



                if (tb_Turma1.nm_turma != txtTurma.Text)
                {

                    BD.Enti.tb_turma tb_Turma2 = new BD.Enti.tb_turma();
                    tb_Turma2.nm_turma = txtTurma.Text;
                    tb_Turma2.nm_curso = txtCurso.Text;
                    tb_Turma2.qt_max_alunos = Convert.ToInt32(nudAlunos.Value);

                    Bussi.Vali valid = new Bussi.Vali();
                    valid.Inserir(tb_Turma1);

                    MessageBox.Show("Sucesso");
                }
                else
                {
                    MessageBox.Show("Turma ja existe, tente outra.");
                }
                
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message);
            }           
        }

        
    }
}
