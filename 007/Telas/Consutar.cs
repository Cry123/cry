﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace _007.Telas
{
    public partial class Consutar : Form
    {
        public Consutar()
        {
            InitializeComponent();
        }

        

        private void Label3_Click(object sender, EventArgs e)
        {
            string curso = txtcurso.Text;
            int aluno = Convert.ToInt32(nudalunos.Value);

            Bussi.Vali vali = new Bussi.Vali();
            List<BD.Enti.tb_turma> tb_Turmas = vali.Cons(curso, aluno);

            dgvCons.DataSource = tb_Turmas;
        }

        
    }
}
