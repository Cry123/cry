﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace _007.Telas
{
    public partial class RemoverPorTurma : Form
    {
        public RemoverPorTurma()
        {
            InitializeComponent();
        }

        private void Button1_Click(object sender, EventArgs e)
        {
            int id = Convert.ToInt32(txtId.Text);

            Bussi.Vali vali = new Bussi.Vali();
            BD.Enti.tb_turma tb_Turma = vali.BuscarPorId(id);

            txtcurso.Text = tb_Turma.nm_curso;
            txtturma.Text = tb_Turma.nm_turma;
            nudqt.Value = tb_Turma.qt_max_alunos;
        }

        private void Button2_Click(object sender, EventArgs e)
        {
            int id = Convert.ToInt32(txtId.Text);

            Bussi.Vali vali = new Bussi.Vali();
            vali.ApagarId(id);

            MessageBox.Show("Turma removida com sucesso.");
        }
    }
}
