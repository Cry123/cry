﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace _007.Telas
{
    public partial class ConsutarPorTurmaECurso : Form
    {
        public ConsutarPorTurmaECurso()
        {
            InitializeComponent();
        }

        private void TxtCurso_TextChanged(object sender, EventArgs e)
        {
            string turma = txtTurma.Text;
            string curso = txtCurso.Text;

            Bussi.Vali vali = new Bussi.Vali();
            List<BD.Enti.tb_turma> tb_Turmas = vali.ConsutarPorTurmaECurso(turma,curso);

            dgvCons.DataSource = tb_Turmas;
        }
    }
}
