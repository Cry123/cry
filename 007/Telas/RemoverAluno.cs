﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace _007.Telas
{
    public partial class RemoverAluno : Form
    {
        public RemoverAluno()
        {
            InitializeComponent();
        }

        private void Button1_Click(object sender, EventArgs e)
        {
            int id = Convert.ToInt32(nudBuscarId.Value);

            Bussi.Vali vali = new Bussi.Vali();
            BD.Enti.tb_aluno aluno = vali.BuscarPorIdAluno(id);

            txtAluno.Text = aluno.nm_aluno;
            txtBairro.Text = aluno.ds_bairro;
            nudIdTurma.Value = aluno.id_turma;
            nudNumeroChamada.Value = aluno.nr_chamada;
            txtMunicipio.Text = aluno.ds_municipio;
            dtpDataNascimento.Value = aluno.dt_nascimento;
        }

        private void Button2_Click(object sender, EventArgs e)
        {
            var id = Convert.ToInt32(nudBuscarId.Value);
            Bussi.Vali vali = new Bussi.Vali();
            vali.ApagarIdAluno(id);

            MessageBox.Show("Removido");
        }
    }
}
