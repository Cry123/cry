﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace _007.Telas
{
    public partial class Menu : Form
    {
        public Menu()
        {
            InitializeComponent();
        }

        private void Button1_Click(object sender, EventArgs e)
        {
            Form1 form1 = new Form1();
            form1.Show();
        }

        private void Button2_Click(object sender, EventArgs e)
        {
            Consutar consutar = new Consutar();
            consutar.Show();
        }

        private void Button3_Click(object sender, EventArgs e)
        {
            ConsutarPorAlunos consutar = new ConsutarPorAlunos();
            consutar.Show();
        }

        private void Button4_Click(object sender, EventArgs e)
        {
            ConsutarPorTurma consutar = new ConsutarPorTurma();
            consutar.Show();
        }

        private void Button5_Click(object sender, EventArgs e)
        {
            ConsutarPorTurmaECurso consutar = new ConsutarPorTurmaECurso();
            consutar.Show();
        }

        private void Button6_Click(object sender, EventArgs e)
        {
            Alterar_Turma alterar_Turma = new Alterar_Turma();
            alterar_Turma.Show();
        }

        private void Button7_Click(object sender, EventArgs e)
        {
            Remover_Turma remover_Turma = new Remover_Turma();
            remover_Turma.Show();
        }
    }
}
