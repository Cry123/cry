﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _007.BD
{
    class BdEscola
    {
        public void Inserir(BD.Enti.tb_turma tb_Turma)
        {
            Enti.schooldbEntities db = new Enti.schooldbEntities();
            db.tb_turma.Add(tb_Turma);

            db.SaveChanges();
        }

        public List<BD.Enti.tb_turma> Cons(string Curso, int alunos)
        {
            Enti.schooldbEntities schooldbEntities = new Enti.schooldbEntities();
            List<Enti.tb_turma> tb_Turmas = schooldbEntities.tb_turma.Where(t => t.nm_curso == Curso && t.qt_max_alunos >= alunos).ToList();

            return tb_Turmas;
        }

        public List<BD.Enti.tb_turma> ConsutarPorAlunos(string Curso, int alunos)
        {
            Enti.schooldbEntities schooldbEntities = new Enti.schooldbEntities();
            List<Enti.tb_turma> tb_Turmas = schooldbEntities.tb_turma.Where(t => t.nm_turma == Curso && t.qt_max_alunos >= alunos).ToList();

            return tb_Turmas;
        }

        public List<BD.Enti.tb_turma> ConsutarPorTurma(string Curso)
        {
            Enti.schooldbEntities schooldbEntities = new Enti.schooldbEntities();
            List<Enti.tb_turma> tb_Turmas = schooldbEntities.tb_turma.Where(t => t.nm_turma == Curso).ToList();

            return tb_Turmas;
        }

        public List<BD.Enti.tb_turma> ConsutarPorTurmaECurso(string Turma, string Curso)
        {
            Enti.schooldbEntities schooldbEntities = new Enti.schooldbEntities();
            List<Enti.tb_turma> tb_Turmas = schooldbEntities.tb_turma.Where(t => t.nm_turma == Turma && t.nm_curso == Curso).ToList();

            return tb_Turmas;
        }

        public List<BD.Enti.tb_turma> Consutar()
        {
            Enti.schooldbEntities schooldbEntities = new Enti.schooldbEntities();
            List<Enti.tb_turma> tb_Turmas = schooldbEntities.tb_turma.ToList();

            return tb_Turmas;
        }

        public void ApagarID(int id)
        {
            Enti.schooldbEntities schooldbEntities = new Enti.schooldbEntities();

            Enti.tb_turma tb_Turma = schooldbEntities.tb_turma.First(t => t.id_turma == id);
            schooldbEntities.tb_turma.Remove(tb_Turma);

            schooldbEntities.SaveChanges();

        }

        public void Alterar(Enti.tb_turma tb_Turma)
        {
            Enti.schooldbEntities schooldbEntities = new Enti.schooldbEntities();

            Enti.tb_turma Turma = schooldbEntities.tb_turma.First(t => t.id_turma == tb_Turma.id_turma);
            Turma.nm_curso = tb_Turma.nm_curso;
            Turma.nm_turma = tb_Turma.nm_turma;
            Turma.qt_max_alunos = tb_Turma.qt_max_alunos;

            schooldbEntities.SaveChanges();
        }

        public BD.Enti.tb_turma BuscarPorId(int id)
        {
            Enti.schooldbEntities schooldbEntities = new Enti.schooldbEntities();
            Enti.tb_turma tb_Turmas = schooldbEntities.tb_turma.Where(t => t.id_turma == id).First();
            
            return tb_Turmas;
        }

        public List<BD.Enti.tb_turma> Listar()
        {
            Enti.schooldbEntities schooldbEntities = new Enti.schooldbEntities();
            List<BD.Enti.tb_turma> tb_Turma = schooldbEntities.tb_turma.ToList();

            return tb_Turma;
        }

        public BD.Enti.tb_turma BuscarTurma(string turma)
        {
            
            Enti.schooldbEntities schooldbEntities = new Enti.schooldbEntities();
            Enti.tb_turma tb_Turmas = schooldbEntities.tb_turma.Where(t => t.nm_turma == turma).First();

            return tb_Turmas;
        }

        public void InserirAluno(BD.Enti.tb_aluno tb_Aluno)
        {
            Enti.schooldbEntities db = new Enti.schooldbEntities();
            db.tb_aluno.Add(tb_Aluno);

            db.SaveChanges();
        }

        public List<BD.Enti.tb_aluno> ListarAlunos()
        {
            Enti.schooldbEntities schooldbEntities = new Enti.schooldbEntities();
            List<BD.Enti.tb_aluno> tb_alunos = schooldbEntities.tb_aluno.OrderBy(t => (t.nm_aluno)).ToList();

            return tb_alunos;
        }

        public BD.Enti.tb_aluno BuscarPorIdAluno(int id)
        {
            Enti.schooldbEntities schooldbEntities = new Enti.schooldbEntities();
            Enti.tb_aluno tb_Turmas = schooldbEntities.tb_aluno.Where(t => t.id_aluno == id).First();

            return tb_Turmas;
        }

        public void AlterarAluno(Enti.tb_aluno tb_Aluno)
        {
            Enti.schooldbEntities schooldbEntities = new Enti.schooldbEntities();

            Enti.tb_aluno Aluno = schooldbEntities.tb_aluno.First(t => t.id_aluno == tb_Aluno.id_aluno);         
            Aluno.id_turma = tb_Aluno.id_turma;
            Aluno.nm_aluno = tb_Aluno.nm_aluno;
            Aluno.nr_chamada = tb_Aluno.nr_chamada;
            Aluno.ds_bairro = tb_Aluno.ds_bairro;
            Aluno.ds_municipio = tb_Aluno.ds_municipio;
            Aluno.dt_nascimento = tb_Aluno.dt_nascimento;
            
            schooldbEntities.SaveChanges();
        }

        public void ApagarIDAltera(int id)
        {
            Enti.schooldbEntities schooldbEntities = new Enti.schooldbEntities();

            Enti.tb_aluno aluno = schooldbEntities.tb_aluno.First(t => t.id_aluno == id);
            schooldbEntities.tb_aluno.Remove(aluno);

            schooldbEntities.SaveChanges();
        }
    }
}
