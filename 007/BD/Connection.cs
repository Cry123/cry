﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _007.BD
{
    class Connection
    {
        public MySqlConnection Create()
        {
            string connectionString = "server=localhost;database=schooldb;uid=root";

            MySqlConnection connection = new MySqlConnection(connectionString);
            connection.Open();

            return connection;
        }
    }
}
