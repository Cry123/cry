﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _007.Bussi
{
    class Vali
    {
        public void Inserir(BD.Enti.tb_turma tb_Turma)
        {
            if (tb_Turma.nm_curso == string.Empty)
            {
                throw new ArgumentException("Curso é obrigatório");
            }

            if (tb_Turma.nm_turma == string.Empty)
            {
                throw new ArgumentException("Turma é obrigatório");
            }

            string QtAlunos = Convert.ToString(tb_Turma.qt_max_alunos);
            if (QtAlunos == string.Empty)
            {
                throw new ArgumentException("A qauntidade de alunos é obrigatório");
            }

            if (tb_Turma.qt_max_alunos <= 24)
            {
                throw new ArgumentException("A quantidade maximo de alunos é de 25");
            }

            string turma = tb_Turma.nm_turma;
            if (turma.Contains(" ") == true)
            {
                throw new ArgumentException("O compo de turma não pode conter espaços");
            }

            if (tb_Turma.nm_curso.Length >= 50)
            {
                throw new ArgumentException("25 Caracteres no maximo no compo 'Curso'");
            }

            if (tb_Turma.nm_turma.Length >= 50)
            {
                throw new ArgumentException("25 Caracteres no maximo no compo 'Turma'");
            }



            BD.BdEscola bdEscola = new BD.BdEscola();
            bdEscola.Inserir(tb_Turma);
        }

        public List<BD.Enti.tb_turma> ConstarPorAlunos(string turma, int Alunos)
        {
            BD.BdEscola bdEscola = new BD.BdEscola();
            List<BD.Enti.tb_turma> tb_Turmas = bdEscola.ConsutarPorAlunos(turma, Alunos);

            return tb_Turmas;
        }

        public List<BD.Enti.tb_turma> Cons(string curso, int Alunos)
        {
            BD.BdEscola bdEscola = new BD.BdEscola();
            List<BD.Enti.tb_turma> tb_Turmas = bdEscola.Cons(curso, Alunos);

            return tb_Turmas;
        }

        public List<BD.Enti.tb_turma> ConsutarPorTurma(string curso)
        {
            BD.BdEscola bdEscola = new BD.BdEscola();
            List<BD.Enti.tb_turma> tb_Turmas = bdEscola.ConsutarPorTurma(curso);

            return tb_Turmas;


        }

        public List<BD.Enti.tb_turma> ConsutarPorTurmaECurso(string Turma, string curso)
        {
            BD.BdEscola bdEscola = new BD.BdEscola();
            List<BD.Enti.tb_turma> tb_Turmas = bdEscola.ConsutarPorTurmaECurso(Turma, curso);

            return tb_Turmas;
        }

        public void ApagarId(int id)
        {
            BD.BdEscola bdEscola = new BD.BdEscola();
            bdEscola.ApagarID(id);
        }

        public void Altera(BD.Enti.tb_turma tb_Turma)
        {
            BD.BdEscola bdEscola = new BD.BdEscola();
            bdEscola.Alterar(tb_Turma);
        }

        public BD.Enti.tb_turma BuscarPorId(int id)
        {
            BD.BdEscola bdEscola = new BD.BdEscola();
            BD.Enti.tb_turma tb_Turmas = bdEscola.BuscarPorId(id);

            return tb_Turmas;
        }

        public List<BD.Enti.tb_turma> Listar()
        {
            BD.BdEscola bdEscola = new BD.BdEscola();
            List<BD.Enti.tb_turma> tb_Turmas = bdEscola.Listar();

            return tb_Turmas;
        }

        public BD.Enti.tb_turma BuscarTurma(string turma)
        {
            BD.BdEscola bdEscola = new BD.BdEscola();
            BD.Enti.tb_turma tb_Turmas = bdEscola.BuscarTurma(turma);

            return tb_Turmas;
        }

        public void InserirAluno(BD.Enti.tb_aluno tb_Aluno)
        {          
            BD.BdEscola bdEscola = new BD.BdEscola();
            bdEscola.InserirAluno(tb_Aluno);
        }

        public List<BD.Enti.tb_aluno> ListarAlunos()
        {
            BD.BdEscola bdEscola = new BD.BdEscola();
            List<BD.Enti.tb_aluno> tb_aluno = bdEscola.ListarAlunos();

            return tb_aluno;
        }

        public BD.Enti.tb_aluno BuscarPorIdAluno(int id)
        {
            BD.BdEscola bdEscola = new BD.BdEscola();
            BD.Enti.tb_aluno alunos = bdEscola.BuscarPorIdAluno(id);

            return alunos;
        }

        public void AlteraAluno(BD.Enti.tb_aluno tb_Aluno)
        {
            BD.BdEscola bdEscola = new BD.BdEscola();
            bdEscola.AlterarAluno(tb_Aluno);
        }

        public void ApagarIdAluno(int id)
        {
            BD.BdEscola bdEscola = new BD.BdEscola();
            bdEscola.ApagarIDAltera(id);
        }
    }
}